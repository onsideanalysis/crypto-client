import React, { Component } from 'react';
import './App.css';
import { HandleTableStreamEvent } from './TableStream.js'

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-enterprise';
import DateRenderer from './DateRenderer.js';
import OrderStatusRenderer from './OrderStatusRenderer.js';
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/ag-theme-blue.css';

import WebSocketClient from '@gamestdio/websocket'

const API_SERVER = process.env.REACT_APP_API_SERVER;
const API_ADDRESS = `http://${API_SERVER}`;
const WS_ADDRESS = `ws://${API_SERVER}/ws`;

class App extends Component {
  constructor(props) {
    super(props);

    this.receiveMessage = this.receiveMessage.bind(this);
    this.processOrderData = this.processOrderData.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);

    this.state = {
      orderGridOptions: {
        columnDefs: [
          {
            headerName: 'Orders',
            headerClass: 'title',
            children: [
              {headerName: 'Id', field:'id', hide: true},
              {headerName: 'Ticker', field:'sticker'},
              {headerName: 'Exchange', field:'execution_details.bookmaker'},
              {headerName: 'Price', field:'price', cellRenderer: priceRenderer},
              {headerName: 'Stop Price', field:'stop_price', cellRenderer: priceRenderer},
              {headerName: 'Size', field:'size', cellRenderer: sizeRenderer},
              {headerName: 'Matched', field:'size_matched', cellRenderer: sizeRenderer},
              {headerName: 'Side', field:'bet_side'},
              {headerName: 'Strategy', field:'strategy', editable:true},
              {headerName: 'Type', field:'order_type'},
              {headerName: 'Status', field:'status', cellRenderer: 'orderStatusRenderer'},
              {headerName: 'Placed', field:'placed_time', cellRenderer: 'dateRenderer', sort: 'desc', comparator: dateComparator},
              {headerName: 'Updated', field:'ut', cellRenderer: 'dateRenderer', comparator: dateComparator}
            ]
          }
        ],
        enableColResize: true,
        rowSelection: 'single',
        onCellEditingStopped: this.onCellEditingStopped,
        enableStatusBar: true,
        alwaysShowStatusBar: true,
        enableRangeSelection: true
      },
      tradeGridOptions: {
        columnDefs: [
          {
            headerName: 'Trades',
            headerClass: 'title',
            children: [
              {headerName: 'Price', field:'price', cellRenderer: priceRenderer},
              {headerName: 'Size', field:'size', cellRenderer: sizeRenderer},
              {headerName: 'Time', field:'dt', cellRenderer: 'dateRenderer', sort: 'desc'}
            ]
          }
        ],
        enableColResize: true
      },
      riskGridOptions: {
        columnDefs: [
          {headerName: 'Strategy', field:'strategy', enableValue: true, enableRowGroup: true, rowGroup: true, rowGroupIndex: 1, hide:true},
          {headerName: 'Date', field:'date', enableValue: true, hide: true},
          {headerName: 'Asset', field:'asset', enableValue: true, enableRowGroup: true, width: 190, suppressSizeToFit: true},
          {headerName: 'Position', field:'position', cellRenderer: sizeRenderer, enableValue: true},
          {headerName: 'Avg Price', field:'avg_price', cellRenderer: priceRenderer, enableValue: true},
          //{headerName: 'Exposure $', field:'exposure_usd', cellRenderer: sizeRenderer, aggFunc: 'sum', enableValue: true, cellStyle: {borderLeft: '2px solid #5B9AD0'}},
          {headerName: 'Total PnL $', field:'pnl_total_usd', cellRenderer: sizeRenderer, aggFunc: 'sum', enableValue: true, cellStyle: {borderLeft: '2px solid #5B9AD0'}},
          {headerName: 'Real $', field:'pnl_rlzd_usd', cellRenderer: sizeRenderer, aggFunc: 'sum', enableValue: true},
          {headerName: 'Unreal $', field:'pnl_unrlzd_usd', cellRenderer: sizeRenderer, aggFunc: 'sum', enableValue: true},
          {headerName: 'COD $', field:'pnl_cod_usd', cellRenderer: sizeRenderer, aggFunc: 'sum', enableValue: true},
          {headerName: 'Total PnL ฿', field:'pnl_total_btc', cellRenderer: btcRenderer, aggFunc: 'sum', enableValue: true, cellStyle: {borderLeft: '2px solid #5B9AD0'}},
          {headerName: 'Real ฿', field:'pnl_rlzd_btc', cellRenderer: btcRenderer, aggFunc: 'sum', enableValue: true},
          {headerName: 'Unreal ฿', field:'pnl_unrlzd_btc', cellRenderer: btcRenderer, aggFunc: 'sum', enableValue: true},
          {headerName: 'COD ฿', field:'pnl_cod_btc', cellRenderer: btcRenderer, aggFunc: 'sum', enableValue: true}
        ],
        autoGroupColumnDef: {
          headerName: 'Positions',
          width: 120,
          suppressSizeToFit: true,
          cellRendererParams: {
            suppressCount: true,
          },
          headerClass: 'title'
        },
        enableColResize: true,
        suppressAggFuncInHeader: true,
        deltaRowDataMode: true,
        getRowClass: function(params) {
          if (params.node.level === 0) {
            return 'agg-row';
          }
        }
      },
      frameworkComponents: {
        dateRenderer: DateRenderer,
        orderStatusRenderer: OrderStatusRenderer,
      },
      orderRowData: [],
      tradeRowData: [],
      riskRowData: [],
      orderTable: {},
      riskTable: {},
      dates: [],
      date: undefined
    }
  }

  componentDidMount() {
      this.subscribeToData();
  }

  onOrderGridReady = (params) => {
    this.orderGridApi = params.api;
    if (this.orderGridApi) {
      this.orderGridApi.sizeColumnsToFit();
    }
  }

  onTradeGridReady = (params) => {
    this.tradeGridApi = params.api;
    if (this.tradeGridApi) {
      this.tradeGridApi.sizeColumnsToFit();
    }
  }

  onRiskGridReady = (params) => {
    this.riskGridApi = params.api;
    if (this.riskGridApi) {
      this.riskGridApi.sizeColumnsToFit();
    }
  }

  onSelectionChanged() {
    var newRowData = [];
    var rows = this.orderGridApi.getSelectedRows();
    if (rows && rows.length === 1) {
      if (rows[0].matched_bets) {
        newRowData = rows[0].matched_bets;
      }
    }
    this.setState({tradeRowData: newRowData});
  }

  onCellEditingStopped(event) {
    let row = event.data;
    let orders = {};
    orders[row.account_id] = [row.id];

    let req = {
      trading_user_id: row.trading_user_id,
      orders: orders,
      strategy: row.strategy,
      strategy_descr: row.strategy_descr,
    };
    console.log(req);

    fetch(`${API_ADDRESS}/orders/${row.id}/strategy`, {method: 'POST', body: JSON.stringify(req)})
      .then((resp) => resp.json())
      .then(json => {
        console.log('Success!');
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  subscribeToData() {
    if (window.WebSocket){
      let ws = new WebSocketClient(WS_ADDRESS, undefined, {
        backoff: "fibonacci"
      });
      ws.onmessage = this.receiveMessage;
      ws.onclose = function() {
        console.log('Socket closed');
      }
      ws.onreconnect = function() {
        console.log('Socket reconnected');
      }
    } else {
      console.log('Browser does not support Websockets');
    }
  }

  receiveMessage(data) {
    let parsed = JSON.parse(data.data);
    console.log(parsed);

    if (parsed.table.startsWith('trading_user_orders')) {
      var orderTable = this.state.orderTable;
      HandleTableStreamEvent(parsed, orderTable);
      this.processOrderData(orderTable);
      this.setState({orderTable: orderTable});
    } else if (parsed.table.startsWith('trading_user_risksheet')) {
      var riskTable = this.state.riskTable;
      HandleTableStreamEvent(parsed, riskTable);
      this.processRiskData(riskTable);
      this.setState({riskTable: riskTable});
    }

  }

  processOrderData(table) {
    let newRowData = [];
    for (let id in table) {
      //Filter error rows
      if (table[id].id) {
        newRowData.push(table[id]);
      }
    }
    this.setState({orderRowData: newRowData});
  }

  processRiskData(table) {

    let newDates = [];
    let dateMap = {};
    for (let id in table) {
      dateMap[table[id].date] = true;
    }
    for (let d in dateMap) {
      newDates.push(d);
    }
    newDates.sort();

    let date;
    if (this.state.date === undefined && newDates.length > 0) {
      date = newDates[newDates.length - 1];
      this.setState({date: date});
    } else {
      date = this.state.date;
    }

    let newRowData = [];
    for (let id in table) {
      dateMap[table[id].date] = true;
      if (table[id].date === date && table[id].position !== 0) {
        newRowData.push(table[id]);
      }
    }

    this.setState({riskRowData: newRowData, dates: newDates}, function() {
      if (this.riskGridApi) {
        this.riskGridApi.expandAll();
      }
    });
  }

  handleDateChange(event) {
    this.setState({date: event.target.value}, function(){
      this.processRiskData(this.state.riskTable);
    })
  }

  render() {

    let options = [];
    for (var i=0; i < this.state.dates.length; i++) {
      options.push(<option value={this.state.dates[i]} selected={this.state.dates[i] === this.state.date}>{this.state.dates[i]}</option>);
    }

    return (
        <div
          className="container ag-theme-blue"
          style={{height: '100vh'}}
        >
          <div className="orders">
            <AgGridReact
              enableSorting={true}
              enableFilter={true}
              gridOptions={this.state.orderGridOptions}
              frameworkComponents={this.state.frameworkComponents}
              rowData={this.state.orderRowData}
              onGridReady={this.onOrderGridReady}
              onSelectionChanged={this.onSelectionChanged.bind(this)} />
          </div>
          <div className="trades">
            <AgGridReact
              enableSorting={true}
              enableFilter={true}
              gridOptions={this.state.tradeGridOptions}
              frameworkComponents={this.state.frameworkComponents}
              rowData={this.state.tradeRowData}
              onGridReady={this.onTradeGridReady}
              toolPanelSuppressSideButtons={true} />
          </div>
          <div className="risk">
            <span style={{padding:'0px 8px'}}>Date</span>
            <select onChange={this.handleDateChange}>
              {options}
            </select>
            <AgGridReact
              enableSorting={true}
              enableFilter={true}
              gridOptions={this.state.riskGridOptions}
              deltaRowDataMode={true}
              getRowNodeId={data => (data.strategy + data.asset + data.date)}
              frameworkComponents={this.state.frameworkComponents}
              rowData={this.state.riskRowData}
              onGridReady={this.onRiskGridReady} />
          </div>
        </div>
      );
  }
}

export default App;

function dateComparator(date1, date2) {
  if (!date1) {
    return -1;
  }
  if (!date2) {
    return 1;
  }
  return new Date(date1).getTime() - new Date(date2).getTime();
}

function priceRenderer(params) {
  if (typeof (params.value) === 'number') {
    if (params.value > 0 && params.value < 1) {
      return params.value.toFixed(6);
    } else if (params.value > 1 && params.value < 100) {
      return params.value.toFixed(2);
    } else {
      return params.value.toFixed(0);
    }
  } else {
    return params.value;
  }
}

function sizeRenderer(params) {
  if (typeof (params.value) === 'number') {
    return params.value.toLocaleString(undefined, { maximumFractionDigits: 0 });
  } else {
    return params.value;
  }
}

function btcRenderer(params) {
  if (typeof (params.value) === 'number') {
    return params.value.toFixed(3);
  } else {
    return params.value;
  }
}
