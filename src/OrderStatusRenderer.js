import React, {Component} from 'react';

export default class OrderStatusRenderer extends Component {

    constructor(props) {
        super(props);

        this.stateMap = {
          '1': 'Unmatched',
          '2': 'Partially Matched',
          '3': 'Matched',
          '4': 'Cancelled',
          '5': 'Settled',
          '6': 'Lapsed',
          '7': 'Partially Cancelled',
          '8': 'Voided',
          '9': 'Rejected',
          '10': 'Pending',
          '11': 'Failed',
          '12': 'Deleted',
          '-1': 'Unknown'
        };

        this.state = {
            value: this.createStatus()
        };
    }

    createStatus() {
      if (this.props.value in this.stateMap) {
        return this.stateMap[this.props.value];
      } else {
        return this.props.value;
      }
    }

    render() {
        return (
            <span>{this.state.value}</span>
        );
    }
};
