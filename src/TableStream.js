const IMAGE = 0;
const DELTA = 1;
const HEARTBEAT_STATUS = 2;
const SUBSCRIPTION_FAILED = 3;

export function HandleTableStreamEvent(e, tableObj) {

  var fail = false;
  if (e && e.hasOwnProperty('type') && e.hasOwnProperty('table')) {
    switch (e.type) {
      case IMAGE:
        if (!e.hasOwnProperty('data')) {
          fail = true;
        } else {
          for (var key in tableObj) {
            if (tableObj.hasOwnProperty(key)) {
              delete tableObj[key];
            }
          }
          for (var key in e.data) {
            if (e.data.hasOwnProperty(key)) {
              tableObj[key] = e.data[key];
            }
          }
        }
        break;
      case DELTA:
        if (!e.hasOwnProperty('data')) {
          fail = true;
        } else {
          for (var key in e.data) {
            if (e.data.hasOwnProperty(key)) {
              if (e.data[key] == null) {
                delete tableObj[key];
              } else {
                tableObj[key] = e.data[key];
              }
            }
          }
        }
        break;
      case HEARTBEAT_STATUS:
        //TODO
        fail = true;
        break;
      case SUBSCRIPTION_FAILED:
        //TODO
        fail = true;
        break;
      default:
        fail = true;
    }
  } else {
    fail = true;
  }

  if (fail) {
    console.log('[TableStreamClient] Invalid or unexpected event: \n' + JSON.stringify(e));
  }
}
