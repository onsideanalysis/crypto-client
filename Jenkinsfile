pipeline {
    agent none
    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10', daysToKeepStr: '20', artifactNumToKeepStr: '1', artifactDaysToKeepStr: '60'))
        ansiColor('xterm')
    }
    environment {
        ECRURL = '987812006073.dkr.ecr.eu-west-2.amazonaws.com/stratagem/crypto/client'
    }
    stages {
        stage('Create docker image'){
            when {
                branch "master"
                expression { fileExists("Dockerfile") }
            }
            agent { label 'master' }
            steps {
                script {
                    sh 'env'
                    withDockerRegistry([url: "https://${ECRURL}", credentialsId: 'ecr:eu-west-2:aws']) {
                        docker.build("${ECRURL}", "--pull .")
                    }
                }
            }
        }
        stage('Push docker image'){
            when {
                branch "master"
                expression { fileExists("Dockerfile") }
            }
            agent { label 'master' }
            steps {
                script {
                    withDockerRegistry([url: "https://${ECRURL}", credentialsId: 'ecr:eu-west-2:aws']) {
                        docker.image("${ECRURL}").push('master')
                        docker.image("${ECRURL}").push('latest')
                    }
                }
            }
        }
        stage('Update service'){
            when {
                branch "master"
                expression { fileExists("Dockerfile") }
            }
            agent { label 'master' }
            steps {
		script {
			sh  "docker service update --image=${ECRURL}:master -d=false --with-registry-auth crypto_client"
		}
            }
        }
    }
}

def notify(color) {
    if (env.CHANGE_BRANCH == null) {
        def msg = ""
        def causes = currentBuild.rawBuild.getCauses()
        for (cause in causes) {
            msg = msg + cause.getShortDescription() + "\n"
        }
        cause = null
        causes = null
        if (color == 'danger' || !(msg =~ /Started by upstream project/)) {
            slackSend (color: "${color}", message: "${env.JOB_NAME.replace('%2F', '/')} - ${currentBuild.displayName} ${currentBuild.currentResult.toLowerCase().capitalize()} after ${currentBuild.durationString.replace(' and counting', '')} (<${currentBuild.absoluteUrl}|Open>)\n$msg")
        }
    }
}
