#----------------------------------------------------------
# Step 1: Build React App
#----------------------------------------------------------
FROM node:carbon as react-app-build

# Create app directory
WORKDIR /usr/src/app/

COPY ./package*.json ./
RUN npm install

COPY ./ ./
RUN npm run build

#----------------------------------------------------------
# Step 2: Deploy contents
#----------------------------------------------------------
FROM nginx:1.15-alpine

# Get the front end build from the previous step
COPY --from=react-app-build /usr/src/app/build /usr/share/nginx/html/
