import React, {Component} from 'react';
import {format} from 'date-fns'

export default class DateRenderer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.createDate()
        };
    }

    createDate() {
        return format(new Date(this.props.value), 'YYYY-MM-DD HH:mm');
    }

    render() {
        return (
            <span>{this.state.value}</span>
        );
    }
};
